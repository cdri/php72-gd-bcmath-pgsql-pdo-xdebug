FROM php:7.2-fpm
MAINTAINER Thiago Pereira Rosa <thiagor@engineer.com>

RUN apt-get update \
    && apt-get install -y libpq-dev libpng-dev libjpeg62-turbo-dev libsqlite3-dev

RUN pecl install xdebug \
    && docker-php-ext-enable xdebug

RUN docker-php-ext-install gd bcmath pcntl pgsql pdo_pgsql pdo_sqlite exif
